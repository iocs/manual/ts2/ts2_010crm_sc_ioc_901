require essioc
require mca

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(mca_DIR)/mca_eth.iocsh", "DEVICENAME=TS2-010CRM:EMR-XRS-001, IPADDR=192.168.100.2")
